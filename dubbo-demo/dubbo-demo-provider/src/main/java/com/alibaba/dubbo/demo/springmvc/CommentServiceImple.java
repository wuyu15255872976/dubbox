package com.alibaba.dubbo.demo.springmvc;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by wuyu on 2016/6/8.
 */
public class CommentServiceImple implements CommentService {


    @Override
    public Comment add(@RequestBody Comment comment) {
        return comment;
    }

    @Override
    public Comment get(@PathVariable("id") Integer id) {
        return new Comment(id, null);
    }

    @Override
    public Comment delete(@PathVariable("id") Integer id) {
        return new Comment(id, null);
    }

    @Override
    public Comment update(@RequestBody Comment comment) {
        return comment;
    }
}
