package com.alibaba.dubbo.demo.springmvc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * Created by wuyu on 2016/6/8.
 */
@RequestMapping("/comment")
public interface CommentService {

    @RequestMapping(value = "/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Comment add(@RequestBody Comment comment);

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Comment get(@PathVariable("id") Integer id);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Comment delete(@PathVariable("id")Integer id);

    @RequestMapping(value = "/", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Comment update(@RequestBody Comment comment);

}
