package com.alibaba.dubbo.demo.springmvc;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.common.json.JSONObject;
import com.alibaba.dubbo.common.serialize.Serialization;

import java.io.Serializable;

/**
 * Created by wuyu on 2016/6/8.
 */
public class Comment implements Serializable {


    private Integer id;

    private String message;

    public Comment(Integer id, String message) {
        this.id = id;
        this.message = message;
    }

    public Comment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "id: " + id + " message:" + message;
    }
}
