package com.alibaba.dubbo.demo.consumer;

import com.alibaba.dubbo.demo.springmvc.Comment;
import com.alibaba.dubbo.demo.springmvc.CommentService;
import com.alibaba.dubbo.rpc.protocol.springmvc.message.HessainHttpMessageConverter;
import com.alibaba.dubbo.rpc.protocol.springmvc.support.SpringDecoder;
import com.alibaba.dubbo.rpc.protocol.springmvc.support.SpringEncoder;
import com.alibaba.dubbo.rpc.protocol.springmvc.support.SpringMvcContract;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import feign.Feign;
import feign.Logger;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by wuyu on 2016/6/8.
 */
public class SpringmvcClient {
    public static void main(String[] args) {

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:dubbo-demo-consumer.xml");
        CommentService commentService = ctx.getBean(CommentService.class);

        Comment addComment = commentService.add(new Comment(1, "add Comment"));
        System.err.println("add Comment " + addComment);

        Comment getComment = commentService.get(1);
        System.err.println("get Comment " + getComment.toString());

        Comment deleteComment = commentService.delete(1);

        System.err.println("delete Comment " + deleteComment.toString());

        Comment updateComment = commentService.update(new Comment(1, "update Comment"));
        System.err.println("update Comment " + updateComment.toString());


    }

    @Test
    public void testFeignClient() {
        List<HttpMessageConverter<?>> messageConverters = new RestTemplate().getMessageConverters();
        messageConverters.add(new FastJsonHttpMessageConverter());
        messageConverters.add(new HessainHttpMessageConverter());
        CommentService commentService = Feign.builder()
                .decoder(new SpringDecoder(messageConverters))
                .encoder(new SpringEncoder(messageConverters))
                .contract(new SpringMvcContract())
                .logLevel(Logger.Level.FULL)
                .target(CommentService.class, "http://localhost:8080/");
        Comment comment = commentService.add(new Comment(1, "hello"));
        System.err.println(JSON.toJSONString(comment));
    }
}
