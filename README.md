
#dubbo

>1. 增加 springmvc,jsonrpc,原生thrift rpc,avro rpc组件.

>2. 增强dubbo对docker容器的支持.使其支持以hostname的方式注册服务.但不检查对应的ip是否存在.

>3. 增加none http容器,只注册服务,不导出服务.使其更好的支持springboot.

>4. 增加 安卓消费dubbo服务的支持.



###Dubbo服务代理,自动把dubbo服务转化成rest服务

```
http://git.oschina.net/wuyu15255872976/dubbo-rpc-proxy
```

###example springboot+springcloud+dubbo+docker
```
https://git.oschina.net/wuyu15255872976/micro-services.git
```



###安装

```
mvn install -Dmaven.test.skip=true
```

###增加了2个http容器 jetty9,none
```
<dubbo:protocol name="springmvc" server="jetty9" port="8080" />

 <!-- 如果 server 值为none,只注册服务,不导出服务,由第三方提供rest服务.dubbo消费 第三方rest服务 -->
<dubbo:protocol name="springmvc" server="none" />
```
###example

```
建议使用高版本的springmvc

接口定义,用来指定相关方法地址,请求方法/参数.
@RequestMapping("/comment")
public interface CommentService {

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Comment add(@RequestBody Comment comment);

    @RequestMapping(value = "/{id}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Comment get(@PathVariable("id") Integer id);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Comment delete(@PathVariable("id")Integer id);

    @RequestMapping(value = "/", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE)
    public Comment update(@RequestBody Comment comment);

}
```

###支持消费非dubbo提供的接口


```
<!-- 生成远程服务代理，可以像使用本地bean一样使用userService -->
 <!-- 如果已经服务中心注册了服务, 可以不用url直连,url直连的是未在服务中心注册的-->
<dubbo:reference id="userService" interface="com.vcg.UserService" protocol="springmvc" url="springmvc://提供服务的server,可以非dubbo服务端" />
```

###更好的支持springmvc
```
由于dubbo-springmvc提供的rest服务,有诸多限制.例如不能使用相关拦截器 .这时候可以由自己提供相关 springmvc 的rest服务,由dubbo消费端负责消费
只需配置提供的协议是springmvc 服务为none即可,只注册相关服务,但不导出相关服务.dubbo消费端可以通过提供的注册地址 ,即可消费自定义的rest服务.
<dubbo:protocol name="springmvc" server="none" />
```

###更好的支持springboot
```
    springboot 与dubbo结合使用,由于springboot已经提供了相关的rest服务,这时候再用dubbo提供相关的rest服务显得有点多余.
    但是 我们可以使用dubbo的注册中心,把相关提供服务的机器注册进去.这样dubbo消费端就可以消费springboot rest提供的相关服务了
    <dubbo:protocol name="springmvc" server="none" />

    spring-boot-starter-dubbo git: https://git.oschina.net/wuyu15255872976/spring-boot-starter-dubbo.git
```

###更好的支持docker
>1. 修改了dubbo不检查对应的ip地址是否存在,如果有相关hostname 会自动绑定所有地址.
>2. 由于dubbo运行在docker容器中,会注册docker的私有地址,造成外部系统无法访问,只能以hostname方式来注册相关地址,dubbo 会检查hostname对应的ip地址.
>3. 如果hostname没有对应的ip,直接报绑定失败.docker 的ip地址是自动分配的,无法固定的添加hostname对应的ip地址.



###dubbo-springmvc 消费端的请求拦截器
```
可以实现feign.RequestInterceptor 接口,做一些请求前的认证,默认提供了feign.auth.BasicAuthRequestInterceptor

关于注册拦截器
     只需让spring bean容器管理即可,默认会拿取所有实现RequestInterceptor的bean
```

例如
>1. 需要携带用户名密码访问.
>2. 需要header里携带相关token
>3. 需要oauth第三方授权.

###支持安卓调用 dubbo-springmvc或dubbox-rest对外提供的接口

```
    http://git.oschina.net/wuyu15255872976/dubbo-android
```

#依赖jar
````
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-validator</artifactId>
    <version>4.3.1.Final</version>
</dependency>

<dependency>
    <groupId>com.netflix.feign</groupId>
    <artifactId>feign-core</artifactId>
    <version>8.16.2</version>
</dependency>

<!-- 建议使用高版本 4.2.6 低版本对ResponseBody 直接只支持方法 -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>3.2.9.RELEASE</version>
</dependency>

<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-core</artifactId>
    <version>2.3.3</version>
</dependency>
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.3.3</version>
</dependency>

<!-- 如果要使用tomcat server -->
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-core</artifactId>
    <version>8.0.11</version>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-logging-juli</artifactId>
    <version>8.0.11</version>
    <scope>compile</scope>
</dependency>

<!-- 如果要使用jett9 server -->
<dependency>
    <groupId>org.eclipse.jetty.aggregate</groupId>
    <artifactId>jetty-all</artifactId>
    <version>9.2.15.v20160210</version>
</dependency>
		
		
		
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
</dependency>

<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.12</version>
</dependency>
```
